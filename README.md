## Description

This repository contains:

* A ddev-based Drupal 10 site with domain lt.ddev.site
* [tests/gatling](./tests/gatling) - A simulation to run Drupal login flow with Gatling
* [tests/k6](./tests/k6) - A simulation to run Drupal login flow with K6


Note: you can run this test on any Drupal site. See [tests/gatling/README.md](./tests/gatling/README.md) or [tests/k6/README.md](./tests/k6/README.md) for details.

## ddev

```
ddev start
ddev composer install
ddev drush site:install --account-name=admin --account-pass=admin -y
```
