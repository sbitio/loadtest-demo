import io.gatling.core.Predef._
import io.gatling.http.Predef._
//import io.gatling.core.structure.ScenarioBuilder

//import java.util.Properties
//import java.io.FileNotFoundException
//import scala.collection.mutable.HashMap
import scala.concurrent.duration._
//import scala.io.Source

class Login extends Simulation {

  // Capture parameters to variables
  val baseUrl = System.getProperty("baseUrl")
  val drupalUser = System.getProperty("drupalUser", "admin")
  val drupalPass = System.getProperty("drupalPass", "admin")

  val rUsers = Integer.getInteger("users", 1).toInt
  val rSeconds = Integer.getInteger("ramp", 1).toInt

  // Prepare the protocol
  val httpProtocol = http
    .baseUrl(baseUrl)
    .acceptHeader("*/*")
    .userAgentHeader("Gatling")
    .inferHtmlResources(AllowList(), DenyList(""".*\.(css|js|gif|jpeg|jpg|ico|woff|woff2|ttf|otf|png|svg)(\?.*)?"""))
    .disableFollowRedirect

  val step00 = http("00 - GET  /")
    .get("/")
    .check(
      status.is(200),
    )
  val step01 = http("01 - GET  /user/login")
    .get("/user/login")
    .check(
      status.is(200),
      css("#user-login-form input[name=form_id]", "value").saveAs("step01_form_id"),
      css("#user-login-form input[name=form_build_id]", "value").saveAs("step01_form_build_id")
    )
  val step02__formData: Seq[(String, String)] = Seq(
    ("form_id", "#{step01_form_id}"),
    ("form_build_id", "#{step01_form_build_id}"),
    ("name", drupalUser),
    ("pass", drupalPass),
  )
  val step02 = http("02 - POST  /user/login")
    .post("/user/login")
    .formParamSeq(step02__formData)
    .check(
      status.is(303),
      header("Location").saveAs("step02_location")
    )
  val step03 = http("03 - GET  /user/ID?check_logged_in=1")
    .get("#{step02_location}")
    .check(
      status.is(200)
    )

  val scn = scenario("Login")
      .exec(step00)
        .pause(1.seconds)
      .exec(step01)
        .pause(1.seconds)
      .exec(step02)
        .pause(1.seconds)
      .exec(step03)
        .pause(1.seconds)

  // Run scenario
  setUp(
    scn.inject(rampUsers(rUsers) during (rSeconds seconds)),
  ).protocols(httpProtocol)
}
