#!/bin/bash

CWD="$( cd "$( dirname $(realpath "${BASH_SOURCE[0]}") )" && pwd )"
cd $CWD

#if [[ -z "${JAVA_OPTS}" ]]; then
#  JAVA_OPTS="-Xms2G -Xmx2G"
#fi

docker run -it --rm --entrypoint= \
  --user "$(id -u)":"$(id -g)" \
  -v $(pwd)/conf:/opt/gatling/conf \
  -v $(pwd)/results:/opt/gatling/results \
  -v $(pwd)/user-files:/opt/gatling/user-files \
  -v $(pwd)/env.properties:/opt/gatling/env.properties \
  --net host \
  ladamalina/gatling:v3.9.5 \
  bash /opt/gatling/bin/gatling.sh \
  -rm local \
  -rf /opt/gatling/results \
  -sf /opt/gatling/user-files \
  --extra-run-jvm-options "$JAVA_OPTS $JAVA_EXTRA_OPTS" \
  "$@"
