# Description

Gatling directory layout and launcher script to run simulations via docker.

It includes a basic simulation for standard a Drupal login flow.

The simulation accepts this parameters:

 * baseUrl - mandatory
 * drupalUser - default "admin"
 * drupalPass - default "admin"
 * users - default 1
 * ramp - default 1

## Usage

- Variables for the simulation are passed as java properties in `JAVA_EXTRA_OPTS` environment variable.
- run.sh pass parameters directly to Gatling executable

Example usage:

```
JAVA_EXTRA_OPTS="-DbaseUrl=https://lt.ddev.site" ./run.sh -s Login
JAVA_EXTRA_OPTS="-DbaseUrl=https://lt.ddev.site -Dusers=1000 -Dramp=10" ./run.sh -s Login
```
