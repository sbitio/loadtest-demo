#/bin/bash

CWD="$( cd "$( dirname $(realpath "${BASH_SOURCE[0]}") )" && pwd )"
cd $CWD

docker run -it --rm \
  --user "$(id -u)":"$(id -g)" \
  -v "$(pwd)":/repo \
  -w /repo \
  --net host \
  docker.io/grafana/k6 run \
  "$@"
