import exec from 'k6/execution'
import http from "k6/http"
import { check, sleep } from "k6"


// This will export to HTML as filename "result.html" AND also stdout using the text summary
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
import { textSummary } from "https://jslib.k6.io/k6-summary/0.0.1/index.js";
export function handleSummary(data) {
  return {
    "results/result.html": htmlReport(data),
    stdout: textSummary(data, { indent: " ", enableColors: true }),
  };
}

export const options = {
  vus: 1,
  userAgent: 'K6',
  insecureSkipTLSVerify: true,
  maxRedirects: 0,
};

export default function () {
  // Variables
  const baseUrl = __ENV.BASEURL
  const drupalUser = __ENV.DRUPAL_USER || "admin"
  const drupalPass = __ENV.DRUPAL_PASS || "admin"

  // Step 00
  let res = http.get(baseUrl)
  check(res, { "status was 200": r => r.status === 200 })
  sleep(1)

  // Step 01
  res = http.get(`${baseUrl}/user/login`)
  check(res, { "status was 200": r => r.status === 200 })
  const step01_form_id = res.html().find('#user-login-form input[name=form_id]').attr('value');
  const step01_form_build_id = res.html().find('#user-login-form input[name=form_build_id]').attr('value');
  sleep(1)

  // Step 02
  let step02__formData = {
    "form_id": step01_form_id,
    "form_build_id": step01_form_build_id,
    "name": drupalUser,
    "pass": drupalPass,
  }
  res = http.post(`${baseUrl}/user/login`, step02__formData);
  check(res, { "status was 303": r => r.status === 303 })
  const step02_location = res.headers['Location']
  sleep(1)

  // Step 03
  res = http.get(step02_location)
  check(res, { "status was 200": r => r.status === 200 })
  sleep(1)
}
