# Description

K6 scenarios and launcher script to run simulations via docker.

It includes a basic simulation for standard a Drupal login flow.

The simulation accepts this parameters:

 * baseUrl - mandatory
 * drupalUser - default "admin"
 * drupalPass - default "admin"
 * users - default 1
 * ramp - default 1

## Usage

- Variables for the simulation are passed as java properties in `JAVA_EXTRA_OPTS` environment variable.
- run.sh pass parameters directly to Gatling executable

Example usage:

```
./run.sh -e BASEURL="https://lt.ddev.site" simulations/login.js
./run.sh -e BASEURL="https://lt.ddev.site" --vus 1000 simulations/login.js
```
